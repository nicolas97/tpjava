package tp2;

public class Cercle extends Forme {
	private double rayon;
	Cercle() {
		super.seDecrire();
		rayon =1.0;
	}
	double getRayon() {
		return rayon;
	}
	void setRayon(double r) {
		rayon = r;
	}
	String seDrecrireCercle() {
		return "un cerclede rayon "+rayon+super.seDecrire();
	}
	Cercle(double r) {
		rayon =r;
	}
	Cercle(double r , String couleur, boolean coloriage) {
		super(couleur,coloriage);
		rayon=r;
	}
	double calculerAire() {
		return rayon*rayon*Math.PI;
	}
	double calculerPerimetre() {
		return 2*Math.PI*rayon;
	}
	}
