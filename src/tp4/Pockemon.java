package tp4;
public class Pockemon {
	//attributs
	public String nom;
	public String type;
	public int puissance,energie,cycles,maxenergie; 
	//constructeurs
	Pockemon(String nom) {
		this.nom=nom;
		maxenergie=50+(int)(Math.random()*((90-50)+1));
		energie= 30+(int)(Math.random()*((maxenergie-30)+1));
		puissance=3+(int)(Math.random()*((10-3)+1));
	}
	//acceseurs
	String getNom() {
		return nom;
	}
	int getEnergie() {
		return energie;
	}
	int getPuissance() {
		return puissance;
	}
	//methodes
	void sePresenter() {
		System.out.println(" je suis " + nom +" ,j'ai " + energie + " points d'energie ("+maxenergie+"max)" + "et une puissance de "+ puissance);
	}
	void manger() {
		this.energie = energie +10 +(int)(Math.random()*((30-10)+1));
		if(energie>maxenergie){
			energie=maxenergie;
	}
	}
	void vivre() {
		this.energie=energie-(20+(int)(Math.random()*((40-20)+1)));
	if(energie<0) {
		energie=0;
	}
	}
	
	boolean isAlive() {
	if(energie>0) {
		return true ;
	}
		else return false;
	}
	void cycles() {
		cycles=0;
	while (energie>0) {
		manger();
		vivre();
		cycles++;
	}
		System.out.println(getNom()+"a vecu" + cycles + "cycles");
	}
	void perdreEnergie(int perte) {
		this.energie=energie-perte;
	}
	void attaquer(Pockemon adversaire) {
		adversaire.perdreEnergie(puissance);
	}
	}
	


